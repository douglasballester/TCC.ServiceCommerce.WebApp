(function(){
	"use strict";

	angular.module("serviceCommerce", [
		"ui.router",
		"ui.utils.masks",
		"ui.mask",
		"ui.select",
		"ngSanitize",
		"serviceCommerce.config"
	]);
})();
