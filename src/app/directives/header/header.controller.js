(function () {
    "use strict";
    angular.module("serviceCommerce")
        .controller("headerController", ["$scope", "$state",
            function ($scope, $state) {
                $scope.logout = logout;

                function logout() {
                    $state.go('logout');
                };
            }
        ]);

})();