(function () {
    "use strict";

    angular.module("serviceCommerce")
        .directive("serviceCommerceHeader", [
            "usuarioService",
            "storageService",
            "autenticacaoService",
            function (usuarioService,
                storageService,
                autenticacaoService) {
                return {
                    restrict: "E",
                    transclude: true,
                    replace: true,
                    scope: true,
                    templateUrl: "app/directives/header/serviceCommerce-header.html",
                    link: function () { },
                    controller: ["$rootScope", "$scope", "$state", function ($rootScope, $scope, $state) {
                        $scope.logout = logout;
                        $scope.editUsuario = editUsuario;
                        $scope.editPerfil = editPerfil;
                        $scope.viewPerfil = viewPerfil;

                        _verifyLoggedIn();
                        $rootScope.$on("verifyLoggedIn", function () {
                            _verifyLoggedIn();
                        });

                        function _verifyLoggedIn() {
                            var userInfo = storageService.getUserInfo();
                            $scope.existeUsuarioLogado = !!userInfo;
                            if (userInfo) {
                                $scope.id = userInfo.id;
                                $scope.nome = userInfo.nome;
                                $scope.email = userInfo.email;
                            }
                        }

                        function viewPerfil(id) {
                            $state.go('root.perfil', { id });
                        }

                        function editUsuario(id) {
                            $state.go('root.editarUsuario', {id});
                        }

                        function editPerfil(id) {
                            $state.go('root.editarPerfil', { id });
                        }

                        function logout() {
                            $state.go('logout');
                        }
                    }]
                };
            }
        ]);
})();
