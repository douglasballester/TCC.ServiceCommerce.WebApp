(function () {
    'use strict';

    angular.module('serviceCommerce').directive('masterpage',[
        "$rootScope", "$location",
        function($rootScope, $location) {
        return {
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: true,
            templateUrl: 'app/directives/master/masterpage.html',
            link: function (scope, element) {
                scope.sidebarOpen = $location.$$path === '/';
                $rootScope.$on('sidebar-change', function(event, param){
                    scope.sidebarOpen = param.isOpen;
                });
            }
        };
    }]);

})();
