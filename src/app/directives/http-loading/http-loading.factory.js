(function () {
	"use strict";
	/*
	 * @description Http Loading Factory
	 */

	angular.module("serviceCommerce").factory("httpLoading", httpLoading);
	httpLoading.$inject = ["$q", "$rootScope", "ToastrFactory",  "$injector"];

	function httpLoading($q, $rootScope, toastrFactory,  $injector) {

		var setTimeoutLoading;

        var blockCount = 0;

        var blockUI = function blockUI(method) {
            if (blockCount > 0) {
                if (setTimeoutLoading)
                    window.clearTimeout(setTimeoutLoading);
                
                if (method === "GET" || method === "OPTIONS") {
                    setTimeoutLoading = window.setTimeout(function () {
                        $rootScope.$broadcast("loader_show");
                        setTimeoutLoading = undefined;
                    }, 500);
                } else {
                    $rootScope.$broadcast("loader_show");
                    setTimeoutLoading = undefined;
                    
                }
            }

            if (blockCount === 0) {
                if (setTimeoutLoading !== undefined) {
                    window.clearTimeout(setTimeoutLoading);
                    setTimeoutLoading = undefined;
                }
                
                window.setTimeout(function () {
                    if (blockCount < 1) {
                        $rootScope.$broadcast("loader_hide");
                    }
                }, 200);
            }
		};
		
		var possuiNotificacoes = function possuiNotificacoes(response) {
            return response &&
                   response.data.notifications &&
                   response.data.notifications.length > 0;
        };

        var possuiNotificacaoOauth = function possuiNotificacaoOauth(response) {
            return response && response.data.error;
        };

        var handleInternalServerError = function handleInternalServerError(response, toastrFactory) {
            if (response.config.suppressMessage) {
                return;
            }

            toastrFactory.error("Algo não saiu como esperado, tente novamente ou entre em contato com o Administrador.", "Ops");
		};
		
		var handleNotFound = function handleNotFound(response, toastrFactory) {
            toastrFactory.error(Translation.RESPOSTA_INESPERADA_SERVIDOR, Translation.TITULO_ERRO_INESPERADO);
        };

        var handleMethodNotAllowed = function handleMethodNotAllowed(response, toastrFactory) {
            if (response.config.suppressMessage) {
                return;
            }

            if (possuiNotificacoes(response)) {
                angular.forEach(response.data.notifications, function (notification) {
                    toastrFactory.error(notification.message);
                });
            }

            $injector.get('$state').transitionTo('root.home');
		};
		
		var handleForbidden = function handleForbidden(response, toastrFactory) {
            if (response.config.suppressMessage) {
                return;
            }

            toastrFactory.error(Translation.USUARIO_SEM_PERMISSAO, Translation.TITULO_ERRO_INESPERADO);
            $injector.get('$state').transitionTo('root.home');
		};
		
		var handleUnauthorized = function handleUnauthorized(response, refreshTokenService) {
            return refreshTokenService.handleAutenticationError(response);
        };

        var handleUnexpected = function handleUnexpected(response, toastrFactory) {
            if (response.config.suppressMessage) {
                return;
            }

            toastrFactory.error(Translation.RESPOSTA_INESPERADA_SERVIDOR, Translation.TITULO_ERRO_INESPERADO);
        };

		var handleBadRequest = function handleBadRequest(response, toastrFactory) {
            if (response.config.suppressMessage) {
                return;
            }

            if (response.config.responseType === 'arraybuffer') {
                var decodedString = String.fromCharCode.apply(null, new Uint8Array(response.data));
                response.data.notifications = JSON.parse(decodedString).notifications;
            }

            if (possuiNotificacoes(response)) {
                angular.forEach(response.data.notifications, function (notification) {
                    toastrFactory.error(notification.message);
                });
            }
            else if (possuiNotificacaoOauth(response)) {
                if (response.data.error === "invalid_grant") {
                    toastrFactory.error(response.data.error_description);
                }
            }
		};
		
		var handleOk = function handleOk(response, toastrFactory) {
            if (response.config.suppressMessage) {
                return;
            }

            if (possuiNotificacoes(response)) {
                angular.forEach(notificacoes, function (notification) {
                    toastrFactory.error(notification.message);
                });
            }
		};
		
		var handleResponse = function handleResponse(response) {
			// debugger;
            var toastrFactory = $injector.get('ToastrFactory');
			// var refreshTokenService = $injector.get('refreshToken.service');
			
            switch (response.status) {
                case 500:
                case -1:
                    handleInternalServerError(response, toastrFactory);
                    break;

                case 200:
                    handleOk(response, toastrFactory);
                    break;

                case 400:
                    handleBadRequest(response, toastrFactory);
                    break;

                case 401:
                    return handleUnauthorized(response, refreshTokenService);

                case 404:
                    handleNotFound(response, toastrFactory);
                    break;

                case 405:
                    handleMethodNotAllowed(response, toastrFactory);
                    break;

                case 403:
                    handleForbidden(response, toastrFactory);
                    break;

                default:
                    handleUnexpected(response, toastrFactory);
                    break;
            }
            return $q.reject(response);
        };


		return {

            request: function (config) {
                if (!config.supressBlockUI) {
                     blockCount++;
                     blockUI(config.method);
                }
                return config;
            },

            response: function (response) {
                if (!response.config.supressBlockUI) {
					 blockCount--;
                     blockUI();
                }
                handleResponse(response);

                return response;
            },

            requestError: function (rejection) {
                if (!rejection.config.supressBlockUI) {
                    blockCount--;
                    blockUI();
                }
                handleResponse(rejection);

                return $q.reject(rejection);
            },

            responseError: function (rejection) {
                if (!rejection.config.supressBlockUI) {
                    blockCount--;
                    blockUI();
                }

                return handleResponse(rejection);
            }
        };
	}
})();
