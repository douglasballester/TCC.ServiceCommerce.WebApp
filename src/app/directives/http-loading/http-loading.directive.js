(function(){
	"use strict";
	/*
	 * @description Http Loading
	 */

	angular.module("serviceCommerce")
	.directive("httpLoading", [
		"$rootScope",
		"SETTINGS",
		"$timeout",
		 function ($rootScope, SETTINGS, $timeout) {
			return{
				link: function ($scope, element) {

			        $scope.$on("loader_show", function () {
                        element.stop().fadeIn();
			        });

			        $scope.$on("loader_hide", function () {
			            element.stop().fadeOut();
			        });
			    }
			};
		}
	]);

})();
