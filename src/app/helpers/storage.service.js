(function () {
    "use strict";

    angular.module("serviceCommerce")
        .service("storageService", StorageService);

    function StorageService() {

        var _authTokenKey = "authToken";
        var _userInfoKey = "userInfo";
        var _userMenuKey = "userMenu";
        var _ultimoPerfilAcessadoKey = "lastPerfil";
        var _localStorage = localStorage;
        var _storage = _localStorage;

        this.setUserInfo = setUserInfo;
        this.getUserInfo = getUserInfo;
        this.removeUserInfo = removeUserInfo;
        this.setAuthToken = setAuthToken;
        this.getAuthToken = getAuthToken;
        this.removeAuthToken = removeAuthToken;
        this.updateAuthToken = updateAuthToken;
        this.set = set;
        this.get = get;
        this.remove = remove;
        this.clear = clear;
        this.setStorage = setStorage;
        this.getUserMenu = getUserMenu;
        this.setUserMenu = setUserMenu;
        this.setUltimoPerfilVisualizado = setUltimoPerfilVisualizado;
        this.getUltimoPerfilVisualizado = getUltimoPerfilVisualizado;

        function setUltimoPerfilVisualizado(perfilId) {
            _localStorage.setItem(_ultimoPerfilAcessadoKey, JSON.stringify(perfilId));
        }

        function getUltimoPerfilVisualizado() {
            return JSON.parse(_localStorage.getItem(_ultimoPerfilAcessadoKey));
        }

        function setUserInfo(data) {
            _localStorage.setItem(_userInfoKey, JSON.stringify(data));
        }

        function getUserInfo() {
            return JSON.parse(_localStorage.getItem(_userInfoKey));
        }

        function removeUserInfo() {
            return _localStorage.removeItem(_userInfoKey);
        }

        function setUserMenu(data) {
            _localStorage.setItem(_userMenuKey, JSON.stringify(data));
        }

        function getUserMenu() {
            return JSON.parse(_localStorage.getItem(_userMenuKey));
        }

        function removeUserMenu() {
            return _localStorage.removeItem(_userMenuKey);
        }

        function setAuthToken(data) {
            _localStorage.setItem(_authTokenKey, JSON.stringify(data));
        }

        function getAuthToken() {
            return JSON.parse(_localStorage.getItem(_authTokenKey));
        }

        function removeAuthToken() {
            return _localStorage.removeItem(_authTokenKey);
        }

        function updateAuthToken(token) {
            removeAuthToken();
            setAuthToken(token);
        }

        function set(key, data) {
            _storage.setItem(key, data);
        }

        function get(key) {
            return _storage.getItem(key);
        }

        function remove(key) {
            _storage.removeItem(key);
        }

        function clear() {
            _storage.clear();
        }

        function setStorage(storage) {
            // Utiliza o localStorage como fallback caso seja infomado um valor null ou undefined
            _storage = storage || _localStorage;
        }
    }
})();