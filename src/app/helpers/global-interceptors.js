(function(){
	"use strict";
	/*
	 * @description httploading config register
	 */
	 angular.module("serviceCommerce")
	.config(["$httpProvider", function ($httpProvider) {
		$httpProvider.interceptors.push("httpLoading");
	}]);

	angular.module('serviceCommerce')
        .config(['$qProvider', function ($qProvider) {
            $qProvider.errorOnUnhandledRejections(false);
        }]);
	 
})();