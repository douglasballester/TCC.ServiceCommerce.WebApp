"use strict";

function UrlBuilder(_baseUrl) {
    var baseUrl = _baseUrl;
    var paths = [];
    var params = [];

    function buildPaths() {
        for (var i = 0; i < paths.length; i++) {
            if (!baseUrl.endsWith('/')) {
                baseUrl += '/';
            }

            baseUrl += paths[i];
        }
    };

    function buildQueryString() {
        if (params.length > 0) {
            baseUrl += '?';

            for (var i = 0; i < params.length; i++) {
                baseUrl += encodeURIComponent(params[i].name) + '=' + encodeURIComponent(params[i].value);

                if (i + 1 < params.length) {
                    baseUrl += '&';
                }
            }
        }
    };

    this.withPath = function (value) {
        if (value !== undefined && value !== null) {
            paths.push(value);
        }
        return this;
    };

    this.withParam = function (name, value) {
        if (value !== undefined && value !== null) {
            if (angular.isArray(value)) {
                value.forEach(function (val) {
                    params.push({
                        name: name,
                        value: val
                    });
                });
            }
            else if (angular.isDate(value)) {
                params.push({
                    name: name,
                    value: moment(value).format('DD/MM/YYYY HH:mm:ss')
                });
            }
            else {
                params.push({
                    name: name,
                    value: value
                });
            }
        }
        return this;
    };

    this.withParamObject = function (paramObject) {
        var self = this;
        if (angular.isObject(paramObject)) {
            Object.keys(paramObject).forEach(function (key) {
                self.withParam(key, paramObject[key]);
            });
        }
        return self;
    };

    this.toString = function () {
        buildPaths();
        buildQueryString();

        return baseUrl;
    };
};
