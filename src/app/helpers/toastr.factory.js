(function(){
	"use strict";
	/*
	 * @description Toastr Factory
	 */
	angular.module("serviceCommerce")
	.factory("ToastrFactory", [
		function () {
			toastr.options = {
				"closeButton": true,
				"debug": false,
				"newestOnTop": false,
				"progressBar": false,
				"positionClass": "toast-top-right",
				"preventDuplicates": true,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "5000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			};
			return {
				success: function (text, title) {
					toastr.success(text, title);
				},
				error: function (text, title) {
					toastr.error(text, title);
				},
				warning: function (text, title) {
					toastr.warning(text, title);
				},
				info: function (text, title) {
					toastr.info(text, title);
				}
			};
		}
   ]);
})();
