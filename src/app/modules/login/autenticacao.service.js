(function () {
    "use strict";

    angular.module("serviceCommerce").service("autenticacaoService", autenticacaoService);
    autenticacaoService.$inject = ["$rootScope", "$http", "$q", "$state", "storageService", "usuarioService", "ToastrFactory", "SETTINGS"];
    function autenticacaoService($rootScope, $http, $q, $state, storageService, usuarioService, toastrFactory, SETTINGS) {
        var _isAuthenticated = false;

        this.autenticarUsuario = autenticarUsuario;
        this.logout = logout;
        this.isAuthenticated = isAuthenticated;
        this.getToken = getToken;

        var _configOAuth = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };

        function _loadAuthToken() {
            var token = storageService.getAuthToken();

            if (token) {
                _addAuthorizationHeader(token);
            }
        };

        function getToken() {
            return $http.defaults.headers.common.Authorization.replace("bearer ", "");
        }

        function isAuthenticated() {
            return _isAuthenticated;
        };

        function _addAuthorizationHeader(token) {
            _isAuthenticated = true;
            $http.defaults.headers.common.Authorization = token.token_type.concat(' ', token.access_token);
        }

        function _deleteAuthorizationHeader() {
            delete $http.defaults.headers.common.Authorization;
        }

        function autenticarUsuario(LoginUsuario) {
            var d = $q.defer();
            var url = new UrlBuilder(SETTINGS.API + "/security/token").toString();
            var data = String.concat("grant_type=password&username=",
                encodeURIComponent(LoginUsuario.usuario),
                "&password=",
                encodeURIComponent(LoginUsuario.senha));

            $http.post(url, data, _configOAuth)
                .then(function (responseToken) {
                    usuarioService.obterUsuarioPeloLogin(LoginUsuario.usuario)
                        .then(function (response) {
                            _addAuthorizationHeader(responseToken.data);
                            storageService.setAuthToken(responseToken.data);
                            storageService.setUserInfo(response.data.usuario);

                            d.resolve(response);
                        });
                })
                .catch(function (response) {
                    console.log(response);
                    d.reject(response);
                });
            return d.promise;
        }

        function logout() {
            //Realiza as regras de logout
            _isAuthenticated = false;
            _deleteAuthorizationHeader();
            storageService.removeAuthToken();
            storageService.removeUserInfo();
            $rootScope.$broadcast("verifyLoggedIn");
            toastrFactory.success("Logout efetuado com sucesso!");
            $state.go('root.home', {}, { reload: true });
        };

        _loadAuthToken();
    }
})();