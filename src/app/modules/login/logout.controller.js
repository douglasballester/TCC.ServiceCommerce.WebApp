(function () {
    "use strict";
    angular.module("serviceCommerce")
        .controller("logoutController", ["$scope", "$stateParams", "ToastrFactory", "autenticacaoService",
            function ($scope, $stateParams, toastrFactory, autenticacaoService) {
                autenticacaoService.logout();
            }
        ]);

})();