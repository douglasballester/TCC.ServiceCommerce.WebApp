(function () {
    "use strict";
    angular.module("serviceCommerce")
        .controller("loginController", ["$rootScope", "$scope", "$state", "ToastrFactory", "autenticacaoService",
            function ($rootScope, $scope, $state, toastrFactory, autenticacaoService) {
                $scope.submitLogin = submitLogin;
                $scope.inputError = "testeClasse";
                function submitLogin(login) {
                    if ($scope.loginform.$valid) {
                        autenticacaoService.autenticarUsuario(login)
                            .then(function (response) {
                                toastrFactory.success("Login efetuado com sucesso!");
                                $rootScope.$broadcast("verifyLoggedIn");
                                $state.go('root.home');
                            })
                            .catch(function (response) {
                                
                            });
                    }
                };
            }
        ]);

})();