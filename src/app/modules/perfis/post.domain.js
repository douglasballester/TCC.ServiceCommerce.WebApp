"use strict";

var CadastroPost = CadastroPost || {};

CadastroPost.Post = function Post() {
    this.id = 0;
    this.userDestination = null;
    this.mensagem = new CadastroMensagem.Mensagem();
};

CadastroPost.Post.prototype.mapperDto = function () {
    return {
        id: this.id,
        userDestination: this.userDestination,
        mensagem: this.mensagem
    };
};

CadastroPost.Post.prototype.mapperView = function (dto) {
    var self = this;

    var c = new CadastroPost.Post();
    c.id = dto.id;
    c.userDestination = dto.userDestination;
    c.mensagem = dto.mensagem;
    
    return c;
};