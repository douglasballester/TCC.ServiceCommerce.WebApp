(function () {
    "use strict";
    angular.module("serviceCommerce")
        .controller("perfilCadastroController", ["$scope", "$q", "$http", "$stateParams", "$state",
            "ToastrFactory", "perfilService", "perfilHabilidadeService",
            "storageService",
            function ($scope, $q, $http, $stateParams, $state,
                toastrFactory, perfilService, perfilHabilidadeService, storageService) {
                $scope.cidades = [];
                $scope.habilidades = [];
                $scope.perfil = {};
                $scope.habilidade = {};
                $scope.filtroHabilidade = {};
                $scope.submitPerfil = submitPerfil;
                $scope.submitPerfilHabilidade = submitPerfilHabilidade;
                $scope.adicionarHabilidade = adicionarHabilidade;
                $scope.removerHabilidade = removerHabilidade;
                $scope.onBlurNumeroDocumento = onBlurNumeroDocumento;


                $scope.perfil.numeroDocumentoIsValid = function () {
                    return $scope.perfil.numeroDocumento && ($scope.perfil.numeroDocumento.length === 11);
                };

                function onBlurNumeroDocumento() {
                    if ($scope.perfil.numeroDocumento) {
                        $scope.perfil.numeroDocumentoIsValid();
                    }
                }

                var habilidades = [];
                function init() {
                    $scope.id = $stateParams.id;

                    if ($stateParams.id === 0) {
                        var defer = $q.defer();
                        var userInfo = storageService.getUserInfo();

                        perfilService.obterPerfil(userInfo.id)
                            .then(function (response) {
                                $scope.perfil = response.data.perfil;
                                _populaHabilidades();
                                defer.resolve();
                            })

                    }

                    if ($stateParams.id > 0) {
                        var defer = $q.defer();
                        perfilService.obterPerfil($stateParams.id)
                            .then(function (response) {
                                $scope.perfil = response.data.perfil;
                                _populaHabilidades()
                                defer.resolve();
                            })
                    }

                    if ($scope.perfil.id === undefined) {
                        $scope.perfil = new CadastroPerfil.Perfil();
                    }
                }

                function _populaHabilidades() {
                    for (var index = 0; index < $scope.perfil.perfilHabilidades.length; index++) {
                        habilidades.push($scope.perfil.perfilHabilidades[index].habilidade);
                    }
                }

                (function carregarCidades() {
                    $http.get("http://servicecommerce.somee.com/api/cidades").then(function (response) {
                        $scope.cidades = response.data.cidades;
                    });
                })();

                (function carregarHabilidades() {
                    $http.get("http://servicecommerce.somee.com/api/habilidades").then(function (response) {
                        $scope.habilidades = response.data.habilidades;
                    });
                })();

                function submitPerfil(perfil) {
                    if ($scope.perfil.id === undefined) {
                        // $scope.perfil.cidade = perfil.cidade.selected;
                        perfilService.incluirPerfil(perfil, $scope.id).then(function (response) {
                            toastrFactory.success("Perfil Criado com sucesso!")
                            $scope.perfil = response.data.perfil;
                        })
                    } else {
                        // $scope.perfil.cidade = perfil.cidade.selected;
                        perfilService.alterarPerfil(perfil).then(function (response) {
                            toastrFactory.success("Perfil atualizado com sucesso!");
                            $scope.perfil = response.data.perfil;
                        })
                    }
                }

                function submitPerfilHabilidade() {
                    console.log($scope.perfil);
                    perfilHabilidadeService.alterarPerfilHabilidade($scope.perfil.perfilHabilidades, $scope.perfil.id).then(function (response) {
                        toastrFactory.success("Habilidades salvas com sucesso!");
                        console.log(response);
                    })
                }

                function adicionarHabilidade(habilidade) {
                    console.log(habilidade.selected);
                    console.log($scope.perfil);
                    
                    if ($scope.perfil.perfilHabilidades === undefined) {
                        $scope.perfil.perfilHabilidades = [];
                    }

                    var jaExiste = habilidades.filter((e) => e.id === habilidade.selected.id);

                    if (jaExiste.length === 0) {
                        var perfilHabilidade = {};
                        perfilHabilidade.habilidade = habilidade.selected;
                        habilidades.push(habilidade.selected);
                        $scope.perfil.perfilHabilidades.push(perfilHabilidade);
                    }
                }

                function removerHabilidade(habilidade) {
                    var filterHabilidades = habilidades.filter(function (el, index, array) {
                        return el.id === habilidade.habilidade.id;
                    });

                    var indexHabilidade = 0;
                    var habilidadeToRemove = 0;
                    for (var index = 0; index < habilidades.length; index++) {
                        var element = habilidades[index].descricao;
                        if (habilidade.habilidade.descricao === element) {
                            indexHabilidade = index;
                        }
                    }

                    habilidades.splice(indexHabilidade, 1);
                    $scope.perfil.perfilHabilidades.splice(indexHabilidade, 1);
                }

                init();
            }
        ]);
})();