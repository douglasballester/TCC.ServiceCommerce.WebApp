(function () {
	"use strict";
	/*
	 * @description Home Controller
	 */

	angular.module("serviceCommerce")
		.controller("listagemPerfilController", ["$scope", "$http", "$stateParams",
			"$state", "storageService", "filtrarPalavrasService",
			"perfilService", "buscarRecomendacoesService",
			function ($scope, $http, $stateParams, $state, storageService, filtrarPalavrasService, perfilService, buscarRecomendacoesService) {
				$scope.filtrarPalavras = filtrarPalavras;
				$scope.fotos = [{
					url: 'http://3.bp.blogspot.com/-AM0MSEPEDTI/T39ld4bhVaI/AAAAAAAABMk/dP9X58GkL6M/s1600/sovcaindanaoviu+%25283%2529.png'
				}, {
					url: 'https://cdn.olhares.pt/client/files/foto/big/276/2765923.jpg'
				}, {
					url: 'https://www.urbanarts.com.br/imagens/produtos/069044/0/Ampliada/3x4-cat.jpg'
				}, {
					url: 'http://4.bp.blogspot.com/-lXz6ID2eA6I/T39lYRYSigI/AAAAAAAABMM/PhKOHbmx51w/s1600/sovcaindanaoviu+%252818%2529.jpg'
				}];

				$scope.refreshResults = refreshResults;
				$scope.embaralhar = embaralhar;
				$scope.habilidades = [];
				$scope.perfis = {};
				$scope.usuarioStorage = storageService.getUserInfo();
				$scope.redirectToPerfil = redirectToPerfil;
				$scope.filtrarPerfis = filtrarPerfis;
				$scope.filtrarPerfisHabilidade = filtrarPerfisHabilidade;
				$scope.recomendacoes = [];
				$scope.filtroHabilidade = {};
				$scope.count = 0;
				$scope.habilidadeParaFiltro = {};

				$scope.clear = function ($event) {
					$event.stopPropagation();
					$scope.filtroHabilidade.selected = undefined;
					$scope.recomendacoes = [];
					perfilService.listarPerfis().then(function (response) {
						$scope.perfis = response.data.perfis;
					});
					$http.get("http://servicecommerce.somee.com/api/habilidades").then(function (response) {
						$scope.habilidades = response.data.habilidades;
					});
					$scope.count = 0;
				};

				function refreshResults($select) {
					var search = $select.search,
						list = angular.copy($select.items),
						FLAG = -1;
					//remove last user input
					list = list.filter(function (item) {
						return item.id !== FLAG;
					});

					if (!search) {
						//use the predefined list
						$select.items = list;
					}
					else {
						//manually add user input and set selection
						var userInputItem = {
							id: FLAG,
							description: search
						};
						$select.items = [userInputItem].concat(list);
						$select.selected = userInputItem;
					}
				}

				function embaralhar() {
					var min = Math.ceil(0);
					var max = Math.floor(3);
					return Math.floor(Math.random() * (max - min + 1)) + min;
				}

				function filtrarPerfisHabilidade(habilidade) {
					//Filtrar Perfis por Habilidade
					filtrarPalavrasService.filtrarPerfis(habilidade).then(function (response) {
						$scope.perfis = response.data.perfis;
					});
				}

				function filtrarPerfis(habilidade) {
					//Filtrar Perfis por Habilidade
					var hab = $scope.habilidadeParaFiltro;
					filtrarPalavrasService.filtrarPerfis(habilidade).then(function (response) {
						$scope.perfis = response.data.perfis;
					}).catch(function (response) {
						$scope.perfis = [];
					});

					buscarRecomendacoesService.buscarRecomendacoes(habilidade).then(function (response) {
						console.log(response.data);
						$scope.recomendacoes = response.data.habilidades;
					});
				}

				function filtrarPalavras($select) {
					console.log($select.selected.description);
					var descr = $select.selected.description || $select.selected.descricao;
					filtrarPalavrasService.filtrarPalavras(descr).then(function (response) {
						$scope.habilidades = response.data.habilidades;
						console.log($scope.habilidades);
					});
					$scope.count++

					if ($scope.count <= 1) {
						$select.activate();
					} else {
						filtrarPerfis($select.selected);
					}

					$scope.habilidadeParaFiltro = $select.selected;
				}

				function redirectToPerfil(id) {
					$state.go('root.perfil', { id });
				}

				(function carregarHabilidades() {
					$http.get("http://servicecommerce.somee.com/api/habilidades").then(function (response) {
						$scope.habilidades = response.data.habilidades;
					});
				})();

				(function carregarPerfis() {
					perfilService.listarPerfis().then(function (response) {
						$scope.perfis = response.data.perfis;
					});
				})();
			}
		]);

})();