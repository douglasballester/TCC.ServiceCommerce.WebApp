"use strict";
angular.module("serviceCommerce").factory("postsService", function ($http, SETTINGS) {
    var _incluirPosts = function (post) {
        var url = new UrlBuilder(SETTINGS.API)
            .withPath("/posts")
            .toString();
        return $http.post(url, {
            post: post
        });
    };

    var _alterarPerfil = function (perfil) {
        var url = new UrlBuilder(SETTINGS.API)
            .withPath("/usuarios")
            .withPath(perfil.usuario.id)
            .withPath("/perfis")
            .withPath(perfil.id)
            .toString();
        return $http.put(url, {
            perfil: perfil,
            usuarioId: perfil.usuario.id
        });
    };

    var _listarPosts = function (id) {
        var url = new UrlBuilder(SETTINGS.API)
            .withPath("/usuarios")
            .withPath(id)
            .withPath("/posts")
            .toString();
        return $http.get(url);
    };

    return {
        incluirPosts: _incluirPosts,
        listarPosts: _listarPosts
    };
});