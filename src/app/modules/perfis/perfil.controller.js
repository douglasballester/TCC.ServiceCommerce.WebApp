(function () {
	"use strict";
	/*
	 * @description Home Controller
	 */

	angular.module("serviceCommerce")
		.controller("perfilController", ["$scope", "$http", "$stateParams", "$state",
										"ToastrFactory", "perfilService", "postsService", "storageService",
			function ($scope, $http, $stateParams, $state, toastrFactory, perfilService, postsService, storageService) {
				$scope.habilidades = {};
				$scope.perfis = {};
				$scope.incluirPost = incluirPost;
				$scope.usuarioStorage = storageService.getUserInfo();
				// $scope.blockPosts = $scope.perfil === undefined;
				$scope.isTheSameUser = $scope.usuarioStorage === null || $stateParams.id === $scope.usuarioStorage.id; 
				$scope.posts = [];
				$scope.redirectToPerfil = redirectToPerfil;
				
				function init() {
					var ultimoPerfilVisitado = $stateParams.id || storageService.getUltimoPerfilVisualizado();  
					storageService.setUltimoPerfilVisualizado(ultimoPerfilVisitado);
					perfilService.obterPerfil(ultimoPerfilVisitado).then(function (response) {
						$scope.perfil = response.data.perfil;
					});

					postsService.listarPosts(ultimoPerfilVisitado).then(function (response) {
						$scope.posts = response.data.posts;
					});
				}

				// (function carregarHabilidades() {
				// 	$http.get("http://servicecommerce.somee.com/api/habilidades").then(function (response) {
				// 		$scope.habilidades = response.data.habilidades;
				// 	});
				// })();
				

				function incluirPost(perfil) {
					console.log(perfil);
					$scope.post = new CadastroPost.Post();
					$scope.post.userDestination = perfil.usuario;

					$scope.mensagem = new CadastroMensagem.Mensagem()
					$scope.mensagem.userSend = 	$scope.usuarioStorage;
					$scope.mensagem.descricao = perfil.post.mensagem.descricao;

					$scope.post.mensagem = $scope.mensagem;
					console.log($scope.post);
					postsService.incluirPosts($scope.post).then(function (response) {
						init();
					});
				}

				function redirectToPerfil(id) {
					console.log('chamou');
					// $state.go('root.editarUsuario', { id });
					$state.go('root.perfil', { id }, {reload: true});
				}

				init();
			}
		]);

})();