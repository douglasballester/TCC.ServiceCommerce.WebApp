"use strict";
angular.module("serviceCommerce").factory("buscarRecomendacoesService", function ($http, SETTINGS) {
    var _buscarRecomendacoes = function (habilidade) {
        var url = new UrlBuilder(SETTINGS.API)
            .withPath("/recomendacoes")
            .withParam("habilidadeId", habilidade.id)
            .toString();
        return $http.get(url);
    };

    return {
        buscarRecomendacoes: _buscarRecomendacoes
    };
});