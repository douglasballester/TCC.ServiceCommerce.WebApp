"use strict";

var CadastroMensagem = CadastroMensagem || {};

CadastroMensagem.Mensagem = function Mensagem() {
    this.id = 0;
    this.userSend = null;
    this.descricao = null;
};

CadastroMensagem.Mensagem.prototype.mapperDto = function () {
    return {
        id: this.id,
        userSend: this.userSend,
        descricao: this.descricao
    };
};

CadastroMensagem.Mensagem.prototype.mapperView = function (dto) {
    var self = this;

    var c = new CadastroMensagem.Mensagem();
    c.id = dto.id;
    c.userSend = dto.userSend;
    c.descricao = dto.descricao;
    
    return c;
};