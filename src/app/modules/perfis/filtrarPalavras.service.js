"use strict";
angular.module("serviceCommerce").factory("filtrarPalavrasService", function ($http, SETTINGS) {
    var _filtrarPalavras = function (palavra) {
        var url = new UrlBuilder(SETTINGS.API)
            .withPath("/palavras")
            .withParam("palavra", palavra)
            .toString();
        return $http.get(url);
    };

    var _filtrarPerfis = function (habilidade) {
        var url = new UrlBuilder(SETTINGS.API)
            .withPath("/habilidades")
            .withPath(habilidade.id)
            .withPath("perfis")
            .toString();
        return $http.get(url);
    };

    return {
        filtrarPalavras: _filtrarPalavras,
        filtrarPerfis: _filtrarPerfis
    };
});