"use strict";

var CadastroPerfil = CadastroPerfil || {};

CadastroPerfil.Perfil = function Perfil() {
    this.id = 0;
    this.numeroDocumento = String.Empty;
    this.cidade = String.Empty;
    this.experienciaProfissional = String.Empty;
    this.sobre = String.Empty;
    this.perfilHabilidades = [];
};

CadastroPerfil.Perfil.prototype.mapperDto = function () {
    return {
        id: this.id,
        numeroDocumento: this.numeroDocumento,
        cidade: this.cidade,
        experienciaProfissional: this.experienciaProfissional,
        sobre: this.sobre,
        perfilHabilidades: this.perfilHabilidades
    };
};

CadastroPerfil.Perfil.prototype.mapperView = function (dto) {
    var self = this;

    var c = new CadastroPerfil.Perfil();
    c.id = dto.id;
    c.numeroDocumento = dto.numeroDocumento;
    c.cidade = dto.cidade;
    c.experienciaProfissional = dto.experienciaProfissional;
    c.sobre = dto.sobre;
    c.perfilHabilidades = dto.perfilHabilidades;

    return c;
};