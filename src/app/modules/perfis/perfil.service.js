"use strict";
angular.module("serviceCommerce").factory("perfilService", function ($http, SETTINGS) {
    var _incluirPerfil = function (perfil, id) {
        var url = new UrlBuilder(SETTINGS.API)
            .withPath("/usuarios")
            .withPath(id)
            .withPath("/perfis")
            .toString();
        return $http.post(url, {
            perfil: perfil,
            usuarioId: id
        });
    };

    var _alterarPerfil = function (perfil) {
        var url = new UrlBuilder(SETTINGS.API)
            .withPath("/usuarios")
            .withPath(perfil.usuario.id)
            .withPath("/perfis")
            .withPath(perfil.id)
            .toString();
        return $http.put(url, {
            perfil: perfil,
            usuarioId: perfil.usuario.id
        });
    };

    var _obterPerfil = function (id) {
        var url = new UrlBuilder(SETTINGS.API)
            .withPath("/usuarios")
            .withPath(id)
            .withPath("/perfis")
            .toString();
        return $http.get(url);
    };

    var _obterUsuarioPeloLogin = function (email) {
        var url = new UrlBuilder(SETTINGS.API)
            .withPath("/usuarios")
            .withParam("email", email)
            .toString();
        return $http.get(url);
    };


    var _listarPerfis = function () {
        var url = new UrlBuilder(SETTINGS.API)
            .withPath("/perfis")
            .toString();
        return $http.get(url);
    };

    return {
        incluirPerfil: _incluirPerfil,
        alterarPerfil: _alterarPerfil,
        obterPerfil: _obterPerfil,
        obterUsuarioPeloLogin: _obterUsuarioPeloLogin,
        listarPerfis: _listarPerfis
    };
});