"use strict";
angular.module("serviceCommerce").factory("perfilHabilidadeService", function ($http, SETTINGS) {
    var _incluirPerfilHabilidade = function (perfilHabilidade, perfilId) {
        var url = new UrlBuilder(SETTINGS.API)
            .withPath("/perfis")
            .withPath(perfilId)
            .withPath("/habilidades")
            .toString();
        return $http.post(url, {
            PerfilHabilidades: perfilHabilidade,
            perfilId: perfilId
        });
    };

    var _alterarPerfilHabilidade = function (perfilHabilidade, perfilId) {
        var url = new UrlBuilder(SETTINGS.API)
            .withPath("/perfis")
            .withPath(perfilId)
            .withPath("habilidades")
            .toString();
        return $http.put(url, {
            PerfilHabilidades: perfilHabilidade,
            perfilId: perfilId
        });
    };

    var _obterPerfil = function (id) {
        var url = new UrlBuilder(SETTINGS.API)
            .withPath("/usuarios")
            .withPath(id)
            .withPath("/perfis")
            .toString();
        return $http.get(url);
    };

    var _obterUsuarioPeloLogin = function (email) {
        var url = new UrlBuilder(SETTINGS.API)
            .withPath("/usuarios")
            .withParam("email", email)
            .toString();
        return $http.get(url);
    };

    return {
        incluirPerfilHabilidade: _incluirPerfilHabilidade,
        alterarPerfilHabilidade: _alterarPerfilHabilidade,
        obterPerfil: _obterPerfil,
        obterUsuarioPeloLogin: _obterUsuarioPeloLogin
    };
});