(function () {
	"use strict";
	/*
	 * @description Home Controller
	 */

	angular.module("serviceCommerce")
		.controller("usuarioController", ["$rootScope", "$scope",
			"$stateParams",
			"ToastrFactory",
			"usuarioService",
			"$location",
			"$q",
			"storageService",
			function ($rootScope, $scope, $stateParams, toastrFactory, usuarioService, $location, $q, storageService) {
				$scope.usuario = null;
				$scope.cadastrarUsuario = cadastrarUsuario;

				function init() {
					$scope.id = $stateParams.id;

					$scope.crudOptions = {
						isNew: !$scope.usuario && !$scope.id
					};

					if (!$scope.crudOptions.isNew) {
						usuarioService.obterUsuario($stateParams.id)
							.then(function (response) {
								// console.log(response);
								$scope.usuario = response.data.usuario;
								// storageService.setUserInfo(response.data.usuario);
							})
					}else{
						$scope.usuario = new CadastroUsuario.Usuario();
					}
				}

				function cadastrarUsuario(usuario) {
					var d = $q.defer();
					if (!usuario.id) {
						usuarioService.incluirUsuario(usuario).then(function (response) {
							$scope.usuario = response.data.usuario;
							toastrFactory.success("Usuário criado com sucesso!");
							$location.path("/login");
						});
					} else {
						usuarioService.alterarUsuario(usuario).then(function (response) {
							$scope.usuario = response.data.usuario;
							toastrFactory.success("Usuário editado com sucesso!");
							storageService.setUserInfo(response.data.usuario);
							$rootScope.$broadcast("verifyLoggedIn");
							d.resolve(response);
							$location.path("/");
						}).catch(function (response) {
							console.log(response);
							d.reject(response);
						});

					}
					return d.promise;
				};

				init();
			}
		]);
})();