"use strict";

var CadastroUsuario = CadastroUsuario || {};

CadastroUsuario.Usuario = function Usuario() {
    this.id = 0;
    this.nome = String.Empty;
    this.email = String.Empty;
    this.senha = String.Empty;
};

CadastroUsuario.Usuario.prototype.mapperDto = function () {
    return {
        id: this.id,
        nome: this.nome,
        email: this.email,
        senha: this.senha
    };
};

CadastroUsuario.Usuario.prototype.mapperView = function (dto) {
    var self = this;

    var c = new CadastroUsuario.Usuario();
    c.id = dto.id;
    c.nome = dto.nome;
    c.email = dto.email;

    return c;
};