"use strict";
angular.module("serviceCommerce").factory("usuarioService", function($http, SETTINGS){
    var _incluirUsuario = function (usuario) {
        var url = new UrlBuilder(SETTINGS.API)
            .withPath("/usuarios")
            .toString();
        return $http.post(url, {
            usuario: usuario
        });
    };

    var _alterarUsuario = function(usuario){
        var url = new UrlBuilder(SETTINGS.API)
            .withPath("/usuarios")
            .withPath(usuario.id)
            .toString();
        return $http.put(url, {
            usuario: usuario
        });
    };

    var _obterUsuario = function(id){
        var url = new UrlBuilder(SETTINGS.API)
            .withPath("/usuarios")
            .withPath(id)
            .toString();
        return $http.get(url);
    };

    var _obterUsuarioPeloLogin = function(email){
        var url = new UrlBuilder(SETTINGS.API)
            .withPath("/usuarios")
            .withParam("email", email)
            .toString();
        return $http.get(url);
    };

    return {
        incluirUsuario: _incluirUsuario,
        alterarUsuario: _alterarUsuario,
        obterUsuario: _obterUsuario,
        obterUsuarioPeloLogin: _obterUsuarioPeloLogin
    };
});