(function () {
	"use strict";
	/*
	 * @description Confguração das rotas com o ui.router
	 */

	angular.module("serviceCommerce")
		.config([
			"$locationProvider",
			"$stateProvider",
			"$urlRouterProvider",
			function ($locationProvider, $stateProvider, $urlRouterProvider) {
				$urlRouterProvider.otherwise("/");
				$stateProvider
				
					.state("root", {
						url: "",
						abstract: true,
						template: "<div ui-view></div>"
					})

					.state("root.home", {
						url: "/",
						// parent: "serviceCommerceHeader",
						templateUrl: "./app/modules/home/home.html",
						controller: "HomeController",
						params: {
							nome: "parametro do controller da home"
						}
					})

					.state("root.login", {
						url: "/login",
						// parent: "serviceCommerceHeader",
						templateUrl: "./app/modules/login/login.html",
						controller: "loginController",
						params: {
							nome: "Login"
						}
					})
					
					.state("root.logout", {
						url: "/logout",
						controller: "logoutController"
					})

					.state("root.listagem", {
						url: "/listagem",
						templateUrl: "./app/modules/perfis/listagem.html",
						controller: "listagemPerfilController",
						params: {
							nome: "perfis"
						}
					})

					.state("root.perfil", {
						url: "/perfil",
						templateUrl: "./app/modules/perfis/perfil.html",
						controller: "perfilController",
						params: {
							nome: "perfil",
							id: undefined,
							reload: false
						}
					})

					.state("root.editarPerfil", {
						url: "/perfil/editar",
						templateUrl: "./app/modules/perfis/editarPerfil.html",
						controller: "perfilCadastroController",
						params: {
							nome: "perfis",
							id: 0
						}
					})

					.state("root.cadastroUsuario", {
						url: "/usuario/cadastro",
						templateUrl: "./app/modules/usuarios/cadastro.html",
						controller: "usuarioController",
						params: {
							nome: "usuarios"
						}
					})
					
					.state("root.editarUsuario", {
						url: "/usuario/editar/{id:int}",
						templateUrl: "./app/modules/usuarios/cadastro.html",
						controller: "usuarioController",
						params: {
							id: 0
						}
					})

					.state("root.historicoContratacao", {
						url: "/usuario/historico",
						templateUrl: "./app/modules/historicos/historicoContratacao.html",
						controller: "historicoController",
						params: {
							nome: "Historico"
						}
					});
				;
			}
		]);

})();
